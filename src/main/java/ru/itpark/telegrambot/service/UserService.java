package ru.itpark.telegrambot.service;

import org.springframework.stereotype.Service;
import ru.itpark.telegrambot.domain.User;
import ru.itpark.telegrambot.repository.UserRepository;

@Service
public class UserService {
    private final UserRepository userRepository;

    public UserService(UserRepository repository, UserRepository userRepository) {
        this.userRepository = userRepository;

    }

    public User findByName(String name) {
        return userRepository.findByName(name);
    }
    public void saveUser(User user){
        userRepository.save(user);
    }
    public void redactUser(String name, User user) {
        userRepository.redactUser(user, name);
    }
}
