package ru.itpark.telegrambot;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.itpark.telegrambot.domain.User;
import ru.itpark.telegrambot.service.UserService;

import java.util.Collections;
import java.util.List;

public class Bot extends TelegramLongPollingBot {

    private UserService userService;


    @Override
    public void onUpdateReceived(Update update) {
        try {
            if (update.hasMessage() && update.getMessage().hasText()) {
                Message inMessage = update.getMessage();
                SendMessage outMessage = new SendMessage();
                outMessage.setChatId(inMessage.getChatId());
//                ищем пользователя и выдаём инфу из базы
                if (inMessage.getText().startsWith("/search")) {
                    if (userService.findByName(inMessage.getText()) != null) {
                        outMessage.setText(userService.findByName(inMessage.getText()).toString());
                    } else {
                        outMessage.setText("данный пользователь не найден");
                    }
                }
//                добавляем нового пользователя
                if (inMessage.getText().startsWith("/save")){
                    String text = inMessage.getText();
                    List<String> save = null;
                    Collections.addAll(save, text.split(" "));
                    userService.saveUser(new User(save.get(1), save.get(2),save.get(3),save.get(4)));
                    outMessage.setText("пользователь добавлен");
                }
//                редактируем пользователя
                if (inMessage.getText().startsWith("/redact")){
                    String text = inMessage.getText();
                    List<String> save = null;
                    Collections.addAll(save, text.split(" "));
                    if (userService.findByName(save.get(1)) != null) {
                        userService.redactUser(save.get(1), new User(save.get(2), save.get(3), save.get(4), save.get(5)));
                        outMessage.setText("пользователь изменён");
                    }

                }
                execute(outMessage);
            }
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onUpdatesReceived(List<Update> updates) {

    }

    @Override
    public String getBotUsername() {
        return "JaI_Iusbot";
    }

    @Override
    public String getBotToken() {
        return "712812262:AAFmdXoVvZsz-MPfy9lfUH6iZwnxs6ke76o";
    }
}
