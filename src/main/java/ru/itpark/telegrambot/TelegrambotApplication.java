package ru.itpark.telegrambot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import ru.itpark.telegrambot.domain.User;
import ru.itpark.telegrambot.service.UserService;

@SpringBootApplication
public class TelegrambotApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(TelegrambotApplication.class, args);
        UserService userService = context.getBean(UserService.class);
        userService.saveUser(new User(  "Олег", "Иванов", "муж", "29"));
        userService.saveUser(new User( "Иван", "Петров", "муж", "21"));
        userService.saveUser(new User(  "Жанна", "Агузарова", "жен", "45"));
        ApiContextInitializer.init();
        TelegramBotsApi telegram = new TelegramBotsApi();
        try {
            telegram.registerBot(new Bot());
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }
}
