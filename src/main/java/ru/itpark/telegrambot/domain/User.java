package ru.itpark.telegrambot.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {
    @Id
    @GeneratedValue
    private int id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private String surname;
    @Column(nullable = false)
    private String gender;
    @Column(nullable = false)
    private String age;

    public User(String name, String surname, String gender, String age) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.age = age;
    }
}
