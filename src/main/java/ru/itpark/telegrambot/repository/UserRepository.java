package ru.itpark.telegrambot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.itpark.telegrambot.domain.User;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query("select u from User u where u.name like %:name%")
    User findByName(@Param("name") String name);

    @Modifying
    @Query("update User u set u = :u where u.name like :name")
    void redactUser (@Param("u") User u, @Param("name") String name);
}
